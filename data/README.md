# Recipe

Окружение для Recipe.

####Внутри:
* ubuntu 14.04
* node 5.x
* nginx
* mongoDB

## Сборка окружения.
```
cd /d/var/www/redux/data
docker build -t recipe .
```

## Запуск окружения.
```
docker run -d -p <docker host>:<www port>:80 -p <docker host>:<ssh port>:22 \
  -e RECIPE_USER=<developer> -e RECIPE_SSH_PUBKEY="$(cat ~/.ssh/id_rsa.pub)" \
  --name recipe-dev recipe
```

`<docker host>` можно посмотреть `ifconfig` в виртуалке докера.
`<www port>` можно ставить 80.
`<ssh port>` можно ставить 22.


## Старт контейнера.
```
docker start recipe-dev
```

## Удалить все.
```
docker rm -f recipe-dev && docker rmi recipe
```

После запуска должно пускать по ssh под `root` и `www-data`:

```
ssh root@<docker host> -p <ssh port>
ssh www-data@<docker host> -p <ssh port>
```

Вся работа с кодом ведётся под юзером `www-data`.

## История изменений.
* 1.0
  * Первый версионный релиз
