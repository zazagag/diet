#!/bin/sh

set -e

if [ -e /.recipe-initialized ]; then
    exit 0
fi

echo "USER=${RECIPE_USER}" >> /var/www/.profile

mkdir -p /var/www/.ssh
mkdir -p /root/.ssh

echo "${RECIPE_SSH_PUBKEY}" >> /var/www/.ssh/authorized_keys
echo "${RECIPE_SSH_PUBKEY}" >> /root/.ssh/authorized_keys

chown www-data:www-data -R /var/www/.ssh
chmod go-rwx -R /var/www/.ssh
chmod go-rwx -R /root/.ssh

touch /.recipe-initialized
