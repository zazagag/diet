export PS1="\[\033[01;33m\]\u@\h\[\033[01;34m\] \w \$\[\033[00m\] "

export LANG=en_US.UTF-8

export HISTCONTROL=ignoredups
export PAGER="less -M +Gg"

export APPLICATION_HOSTNAME=docker

export IS_IN_DOCKER=1

PS1='\[\033[01;36m\]`git branch 2>/dev/null | grep "^* " | sed "s/^* //" | sed "s/anikitin//"` \[\033[01;37m\][\w]\[\033[00m\] \[\033[01;32m\]$\[\033[00m\] '
