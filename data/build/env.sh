#!/bin/sh

set -e

mkdir -p /var/www

cp /build/conf/www.profile /var/www/.profile
echo "cd /var/www/recipe/master" >> /var/www/.profile

echo "export APPLICATION_ROOT=/var/www/recipe/master/" >> /var/www/.profile
echo "export APPLICATION_PROJECT=recipe" >> /var/www/.profile
echo "export APPLICATION_PLATFORM=dev" >> /var/www/.profile
echo "export APPLICATION_REVISION=1" >> /var/www/.profile

mkdir -p /var/www/recipe/master
chown www-data:www-data -R /var/www

echo "www-data ALL=(ALL:ALL) NOPASSWD: ALL" >> /etc/sudoers

mv /build/conf/recipe-init.sh /etc/my_init.d/recipe-init.sh
