#!/bin/sh

set -e

apt-get install -y nginx-extras

echo "daemon off;" >> /etc/nginx/nginx.conf

mv /build/conf/nginx.conf /etc/nginx/sites-available/default
mv /build/conf/nginx.hash.conf /etc/nginx/conf.d/hash.conf
