#!/bin/sh

set -e

apt-get clean

rm -rf /build
rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

chown www-data:www-data -R /var/www
