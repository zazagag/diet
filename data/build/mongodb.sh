#!/bin/sh

set -e

sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927

echo "deb http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list

sudo apt-get update

mkdir -p /data/db

chown www-data:www-data -R /data

sudo apt-get install -y mongodb-org

cp /build/conf/mongod /etc/init.d/mongod

chmod 755 /etc/init.d/mongod
