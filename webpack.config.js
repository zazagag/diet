var path = require('path'),
    webpack = require('webpack'),
    srcPath = path.join(__dirname, 'public/static/src/js'),
    ExtractTextPlugin = require('extract-text-webpack-plugin'),
    destPath = path.join(__dirname, 'public/static'),
    publicPath = '/static/js/',
    cssnext = require('postcss-cssnext'),
    SvgStore = require('webpack-svgstore-plugin');

var isProd = process.env.NODE_ENV === 'prod',
    getLoaders = function() {
        return [{
            test: /\.js|\.jsx$/,
            loaders: ['babel'],
            exclude: /node_modules/,
            include: __dirname,
            cacheDirectory: true,
            plugins: ['transform-decorators-legacy' ],
            presets: ['es2015', 'stage-0', 'react']

        }, {
            test: /\.less$/,
            loader:ExtractTextPlugin.extract(
                isProd ? 'css!less' : 'css?sourceMap!less?sourceMap!postcss-loader'
            )
        }, {
            test: /\.svg$/,
            loader: 'svg-inline'
        }];
    },
    getPlugins = function(isServer) {
        var plugins = [
            new ExtractTextPlugin('styles.css', {allChunks: true}),
            new webpack.NoErrorsPlugin(),
            new webpack.DefinePlugin({
                'server': JSON.stringify(isServer ? true : false)
            }),
            new SvgStore(path.join(__dirname, 'public/svg/items', '**/*.svg'), '../svg/', {
                name: 'icon-sprite.svg'
            })
        ];

        if (isProd) {
            plugins.push(
                new webpack.optimize.UglifyJsPlugin({
                    sourceMap: false,
                    mangle: false,
                    compress: {
                        drop_console: true,
                        drop_debugger: true
                    }
                })
            )
        }

        if(!isServer) {
            plugins.push(
                new webpack.ProvidePlugin({
                    'Promise': 'imports?this=>global!exports?global.Promise!es6-promise', // Thanks Aaron (https://gist.github.com/Couto/b29676dd1ab8714a818f#gistcomment-1584602)
                    'fetch': 'imports?this=>global!exports?global.fetch!whatwg-fetch'
                })
            );
        }

        return plugins;
    };

module.exports = [{
    devtool: 'source-map',
    entry: {
        app: './src/client/index.js'
    },
    output: {
        path: destPath,
        publicPath: '/static/js/',
        filename: '[name].js'
    },
    plugins: getPlugins(),
    module: {
        loaders: getLoaders()
    },
    resolve: {
        root: path.resolve('./src'),
        extensions: ['', '.js', '.jsx', '.less']
    },
    postcss: function () {
        return [
            cssnext({
                browsers: ['last 5 versions']
            })
        ];
    }
}, {
    name: 'server-side rendering',
    entry: {
        'server': './src/server/server.js'
    },
    target: 'node',
    output: {
        path: path.join(__dirname, 'src/server/compiled'),
        filename: '[name].js',
        publicPath: publicPath,
        libraryTarget: 'commonjs2'
    },
    externals: /^[a-z\-0-9]+$/,
    plugins: getPlugins(true),
    module: {
        loaders: getLoaders()
    },
    resolve: {
        root: path.resolve('./src'),
        extensions: ['', '.js', '.jsx', '.less']
    }
}];
