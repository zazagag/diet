#Isomorphic Single App
SAP приложение с использованием *ReactJS*, *Redux*, *ES6(Babel)*.

####*Запуск сервисов.*
```
sudo service mongod start
sudo sv restart nginx
```

####*Клонируем код приложения.*
```
git clone git@github.com:zazagag/redux.git ~/recipe/master
```
   
####*Устанавливаем зависимости и заполняем базу.*    
```
npm run init
```
 
####*Компилируем статику и запускаем сервер.*
#####*dev*
```Webpack``` и ```nodejs``` сервер запускаются в режиме ```--watch```
```
sudo npm run start-dev
```
    
#####*prod*
```
npm run start
```


####*MongoDB*
#####*Dump collection*
```mongoexport --collection recipes --out recipes.json```

#####*Restore collection*
```
mongoimport --collection sections --file ./src/db/init/sections.json
mongoimport --collection recipes --file ./src/db/init/recipes.json
```

#####*Windows mongo.config*
    ##store data
    dbpath=D:\var\mongodb\data
    
    ##Log File
    logpath=D:\var\mongodb\log\mongo.log
    
    ##log read and write operations
    diaglog=3
    
    ##storageEngine
    storageEngine=mmapv1
    
    ##install as service
    ##mongod.exe --config D:\var\mongodb\mongo.config --install

####*Less*

Entry point in `src/js/index.js`:

    import '../less/index.less';

*PostCSS*:
*- Autoprefixes*
