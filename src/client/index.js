import React from 'react';
import { render } from 'react-dom';
import { Router, browserHistory } from 'react-router';
import { Provider } from 'react-redux';
import reducerMenu from 'store/reducers/reducerMenu'
import reducerPage from 'store/reducers/reducerPage'
import reducerError from 'store/reducers/reducerError';
import getRoutes from 'server/routes';
import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunkMiddleware from 'redux-thunk';
import './polyfill.js';

const store = (window.devToolsExtension ? window.devToolsExtension()(createStore) : createStore)(
    combineReducers({
        reducerMenu,
        reducerPage,
        reducerError
    }),
    window.__INITIAL_STATE__,
    applyMiddleware(
        thunkMiddleware
    )
);

render(
    <Provider store={store}>
        <Router history={browserHistory}>
            {getRoutes(store)}
        </Router>
    </Provider>, document.querySelector('#react__node')
);
