import React, { Component, PropTypes } from 'react';
import {Link} from 'react-router';
import './NotFoundPage.less';

class NotFoundPage extends Component {
    render() {
        return (
            <div className="page page--not-found">
                <div className="page__title">Houston! We're have a problem!</div>
                <div className="page__text">404. Page not found.</div>
                <Link to={'/'}>{<img src="/images/teapot.png"/>}</Link>
            </div>
        )
    }
}

export default NotFoundPage;
