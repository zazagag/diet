import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router';
import 'less/index.less';
import Menu from 'components/Menu/Menu.jsx';
import NotFoundPage from 'components/Page/NotFound/NotFoundPage.jsx';
import './AppPage.less';

class AppPage extends Component {
    componentWillMount() {
        console.log('AppPage.componentWillMount');
    
    }
    componentDidMount() {
        console.log('AppPage.componentDidMount');
    }
    componentWillReceiveProps() {
        console.log('AppPage.componentWillReceiveProps');
    }

    componentWillUpdate() {
        console.log('AppPage.componentWillUpdate');
    }

    componentDidUpdate() {
        console.log('AppPage.componentDidUpdate');
    }

    componentWillUnmount() {
        console.log('AppPage.componentWillUnmount');
    }
    
    render() {
        console.log('AppPage.js:39 render');

        const {content, ajaxDone} = this.props;
        if (!content || !content.list) {
            console.log('____AppPage render NotFoundPage');
            return  (
                <NotFoundPage />
            );
        }

        const pageContentBodyClass = 'page__content__body' + (!ajaxDone ? ' filter-blur' : '');

        console.log('____AppPage render normal');
        console.log(pageContentBodyClass);
        return (
            <div className="page">
                <div className="page__header">
                    <Link to="/" />
                </div>
                <div className="page__content__wrap">
                    <div className="page__content__right">
                        <Menu />
                    </div>
                    <div className={pageContentBodyClass}>
                        {this.props.children}
                    </div>
                </div>
                <div className="page__footer">
                    Кулинария - просто!©
                </div>
            </div>
        )
    }
}

export default connect(state => ({
    content: state.reducerPage.content,
    isServer: state.reducerPage.isServer,
    ajaxDone: state.reducerPage.ajaxDone,
    error: state.reducerError.error
}))(AppPage)
