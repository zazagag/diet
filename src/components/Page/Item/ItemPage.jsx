import React, {Component} from 'react';
import * as Actions from 'store/actions';
import {connect} from 'react-redux';
import Meta from 'components/Meta/Meta';
import './ItemPage.less';

class ItemPage extends Component {
    static fetchData(options = {}) {
        const {store, params: {sectionId: sectionId, recipeId: recipeId}} = options;
        console.log('ItemPage fetch');

        fetch(`/api/recipes/${sectionId}/${recipeId}`)
            .then(response => response.json())
            .then((data) => {
                store.dispatch(Actions.receiveRecipeItem(data));
            });
    }

    componentDidUpdate() {
        console.log('ItemPage.componentDidUpdate');
    }
    
    render() {
        const { content } = this.props;

        let title,
            description,
            imagesSrc = [],
            images = [],
            item = content.list[0];

        if (item) {
            title = item.title;
            images = item.images;
            description = item.description.replace(title, '').replace(/^(<br>)+/, '').replace(/(<br>)+$/, '');
        }

        images.forEach((image) => {
            imagesSrc.push(<img src={`/images/items/${image}_160x120.jpg`} key={Math.random().toFixed(4)}/>)
        });

        (new Meta(content.meta)).update();

        const
            svgClassName = `section__icon icon-${content.sectionId}`,
            svgIcon = `icon-${content.sectionId}`,
            useSvgTag = `<use xlink:href="#${svgIcon}" />`;

        return (
            <div className="page__content page__content--item">
                <div className="page__content__header">
                    <svg className={svgClassName} dangerouslySetInnerHTML={{__html: useSvgTag }} />
                    <h2>{content.sectionTitle}</h2>
                </div>
                <h5 className="page__content__title">{title}</h5>
                <div className="page__content__images">{imagesSrc}</div>
                <div className="page__content__desc" dangerouslySetInnerHTML={{__html: description}} />
            </div>
        )
    }
}

export default connect(state => ({
    content: state.reducerPage.content,
    ajaxDone: state.reducerPage.ajaxDone
}))(ItemPage)
