module.exports = {
    componentWillMount: function() {
        this.isLogLifecycle() && console.log('SectionPage.componentWillMount');

    },
    componentDidMount: function() {
        this.isLogLifecycle() && console.log('SectionPage.componentDidMount');
    },
    componentWillReceiveProps: function() {
        this.isLogLifecycle() && console.log('SectionPage.componentWillReceiveProps');
    },
//    shouldComponentUpdate: function() {
//        this.isLogLifecycle() && console.log('SectionPage.shouldComponentUpdate');
//    },
    componentWillUpdate: function() {
        this.isLogLifecycle() && console.log('SectionPage.componentWillUpdate');
    },

    componentDidUpdate: function() {
        this.isLogLifecycle() && console.log('SectionPage.componentDidUpdate');
    },

    componentWillUnmount: function() {
        this.isLogLifecycle() && console.log('SectionPage.componentWillUnmount');
    },

    isLogLifecycle: function() {
        return true;
    },

    isFirstPageLoad: function() {
        return this.props.isServer;
    }
}
