import React, { Component, PropTypes } from 'react';
import {Link} from 'react-router';
import {connect, dispatch} from 'react-redux';
import RecipePreviewItem from 'components/RecipePreviewItem/RecipePreviewItem';
import NotFoundPage from 'components/Page/NotFound/NotFoundPage';
import Paginator from 'components/Paginator/Paginator';
import * as Actions from 'store/actions';
import './SectionPage.less';
import Meta from 'components/Meta/Meta';
import 'components/Paginator/Paginator.less';

class SectionPage extends Component {
    static fetchData(options = {}) {
        console.log('___SectionPage::fetchData_____');
        let {store, params: {sectionId: sectionId}, page} = options;
        store.dispatch(Actions.requestSectionRecipeList());
        fetch(`/api/recipes/${sectionId}/?page=${page || 1}`)
            .then(response => response.json())
            .then((data) => {
                store.dispatch(Actions.receiveSectionRecipeList(data));
            });
    }

    /**
     * Срабатывает когда в роутере меняется GET параметр.
     * @param nextProps
     */
    componentWillReceiveProps(nextProps) {
        console.log('SectionPage:componentWillReceiveProps!!!!!!!');
    }

    componentDidUpdate() {
        console.log('SectionPage.componentDidUpdate');
    }

    render() {
        const {content: {list, page, pages, meta, sectionId, sectionTitle}, location: {pathname}, ajaxDone} = this.props;
        const recipes = [];
        list.forEach((item) => {
            recipes.push(<RecipePreviewItem item={item} key={item._id}/>);
        });

        if (meta) {
            (new Meta(meta)).update();
        }

        const
            svgClassName = `section__icon icon-${sectionId}`,
            svgIcon = `icon-${sectionId}`,
            useSvgTag = `<use xlink:href="#${svgIcon}" />`;

        return (
            <div className="page__content page__content--section">
                <div className="page__content__header">
                    <svg className={svgClassName} dangerouslySetInnerHTML={{__html: useSvgTag }} />
                    <h2>{sectionTitle}</h2>
                </div>
                <Paginator pages={pages} current={page} range={25} pathname={pathname}/>
                <div className="recipe__item__wrap">{recipes}</div>
                <Paginator pages={pages} current={page} range={25} pathname={pathname}/>
            </div>
        )
    }
}

export default connect(state => ({
    content: state.reducerPage.content,
    ajaxDone: state.reducerPage.ajaxDone
}))(SectionPage)
