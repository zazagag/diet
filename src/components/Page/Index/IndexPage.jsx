import React, {Component} from 'react';
import {connect} from 'react-redux';
import RecipePreviewItem from 'components/RecipePreviewItem/RecipePreviewItem.jsx';
import * as Actions from 'store/actions';
import Meta from 'components/Meta/Meta';
import './IndexPage.less';

class IndexPage extends Component {
    static loadFromServer = true;
    static fetchData(options = {}) {
        let {store} = options;
        store.dispatch(Actions.requestIndexRecipeList());
        return fetch(`/api/recipes/`)
            .then(response => response.json())
            .then((list) => {
                console.log('IndexPage::fetchData status 200');
                store.dispatch(Actions.receiveIndexRecipeList(list));
            });
    }

    componentDidUpdate() {
        console.log('IndexPage.componentDidUpdate');
    }
    
    render() {
        const {content} = this.props;
        const recipes = [];
        content.list.forEach((item) => {
            recipes.push(<RecipePreviewItem item={item} key={item._id}/>);
        });

        if (content.meta) {
           (new Meta(content.meta)).update();
        }

        const
            svgClassName = `section__icon icon-raznoe`,
            svgIcon = `icon-raznoe`,
            useSvgTag = `<use xlink:href="#${svgIcon}" />`;

        return (
            <div className="page__content page__content--index">
                <div className="page__content__header">
                    <svg className={svgClassName} dangerouslySetInnerHTML={{__html: useSvgTag }} />
                    <h2>Случайные рецепты</h2>
                </div>
                <div className="recipe__item__wrap">{recipes}</div>
            </div>
        )
    }
}

export default connect(state => ({
    content: state.reducerPage.content,
    ajaxDone: state.reducerPage.ajaxDone
}))(IndexPage)
