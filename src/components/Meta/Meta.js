class Meta {
    constructor(options) {
        this.title = options.title || '';
        this.description = options.description || '';
        this.keywords = options.keywords || '';
    }

    update() {
        if (JSON.parse(server)) {
            return false;
        }

        document.querySelector('title').innerHTML = `${this.title}`;
        document.querySelector('meta[name="description"]').content = `${this.description}`;
        document.querySelector('meta[name="keywords"]').content = `${this.keywords}`;
    }

    getHTML() {
        return `
            <title>${this.title}</title>
            <meta name="keywords" content="${this.keywords}">
            <meta name="description" content="${this.description}">
        `;
    }
}

export default Meta;
