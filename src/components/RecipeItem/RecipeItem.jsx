import React, {Component} from 'react';
import './RecipeItem.less';

class RecipeItem extends Component {
    render() {
        var links = [];
        const {img, title, text} = this.props;
        return (
            <div className="recipe__item">
                <div className="recipe__item__img">
                    <img src={`/images/item/${img}.jpg`} />
                </div>
                <div className="recipe__item__title">{title}</div>
                <div className="recipe__item__text" dangerouslySetInnerHTML={{__html: text}}></div>
            </div>
        );
    }
}

export default RecipeItem;
