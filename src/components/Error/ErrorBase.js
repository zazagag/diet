/**
 * Error base class.
 */
class ErrorBase extends Error {
    /**
     * @constructor
     * @param options
     * @param {string} options.msg message
     * @param {number} options.status status
     * @param {string} options.place place
     */
    constructor(options = {}) {
        const {status, msg, place} = options;
        super(msg);
        this.status = status;
        this.msg = msg;
        this.place = place;
        this.time = '[' + (new Date()).toString().split(' ').slice(0, 5).join(' ') + '] ';
    }

    getStatus() {
        return this.status;
    }

    getMsg() {
        return this.msg;
    }

    getPlace() {
        return this.place;
    }

    getTime() {
        return this.time;
    }

    toString() {
        return [
            this.getTime(),
            this.getPlace(),
            this.getStatus(),
            this.getMsg()
        ].join(':');
    }
}

export default ErrorBase;
