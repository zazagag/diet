import ErrorBase from 'components/Error/ErrorBase';

/**
 * Error not found.
 */
class ErrorNotFound extends ErrorBase {
    /**
     * @constructor
     * @param options
     * @param {string} options.msg message
     * @param {number} options.status status
     * @param {string} options.place place
     */
    constructor(options = {}) {
        super(options);
        this.status = 404;
    }
}

export default ErrorNotFound;
