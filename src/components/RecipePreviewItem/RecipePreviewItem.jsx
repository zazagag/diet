import React, {Component} from 'react';
import {Link} from 'react-router';
import './RecipePreviewItem.less';

class RecipePreviewItem extends Component {
    render() {
        const {_id, id, images, title, description, sectionId} = this.props.item;
        return (
            <div className="recipe-preview__item">
                <Link className="recipe-preview__item__link" to={`/${sectionId}/${id}/`}>
                    <div className="recipe-preview__item__img">
                        <img src={`/images/items/${images[0]}_160x120.jpg`} />
                    </div>
                    <div className="recipe-preview__item__title">
                        {title}
                    </div>
                </Link>
            </div>
        );
    }
}

export default RecipePreviewItem;
