import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';

class MenuLink extends Component {
    render() {
        let className = 'menu__item__link menu__item__link--' + this.props.id + (this.props.selected ? ' menu__item__link--active' : '');
        const svgIcon = `icon-${this.props.id}`,
            useSvgTag = `<use xlink:href="#${svgIcon}" />`;
        return (
            <li className='menu__item' key={this.props.key + 'li'}>
                <Link to={this.props.url} activeClassName="menu__item__link--active" className={className} key={this.props.key + 'link'}>
                    <svg className={'menu__icon ' + svgIcon} dangerouslySetInnerHTML={{__html: useSvgTag }} key={this.props.key + 'svg'}/>
                    {this.props.title}
                </Link>
            </li>
        )
    }
}

export default MenuLink;
