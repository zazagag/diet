import React, { Component, PropTypes } from 'react';
import MenuLink from 'components/Menu/MenuLink.jsx';
import { connect } from 'react-redux';
import './Menu.less';

/**
 *
 */
class Menu extends Component {
    render() {
        let links = [],
            sectionId;

        const {list, selectedId: selectedItem} = this.props;
        if (selectedItem) {
            /**
             * Для страницы с рецептом нужно принудительно передать id раздела.
             * @type {*|sectionId|filter.sectionId|{$in}}
             */
            sectionId = selectedItem.sectionId;
        }

        list.forEach(function(item) {
            links.push(<MenuLink url={item.url} title={item.name} key={item.sectionId} id={item.sectionId} selected={sectionId === item.sectionId}/>);
        });

        return (
            <ul className="menu">{links}</ul>
        );
    }
}

export default connect(state => ({
    list: state.reducerMenu.list,
    selectedId: state.reducerPage.content && state.reducerPage.content.sectionId
}))(Menu)
