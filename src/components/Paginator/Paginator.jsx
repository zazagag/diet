import React, { Component, PropTypes } from 'react';
import {Link} from 'react-router';
import './Paginator.less';

class Paginator extends Component {
    underscoreRange(start, stop) {
        if (arguments.length <= 1) {
            stop = start || 0;
            start = 0;
        }

        var length = Math.max(stop - start, 0);
        var idx = 0;
        var arr = new Array(length);

        while (idx < length) {
            arr[idx++] = start;
            start += 1;
        }

        return arr;
    }

    /**
     * Returns the number of page numbers
     */
    getDisplayCount() {
        if (this.props.range > this.props.pages) {
            return this.props.pages;
        }
        return this.props.range;
    }

    /**
     * Returns a range [start, end]
     */
    getPageRange() {
        const displayCount = this.getDisplayCount();
        const {current, range, pages} = this.props;

        let start = 1;
        if (current >= range) {
            start = (current - range) + 2;
        }

        let stop = start + range;
        if (pages > range && stop > pages) {
            start = pages - range + 1;
            stop = start + displayCount;
        }

        if (stop > pages) {
            stop = pages + 1;
        }

        return this.underscoreRange(start, stop);
    }

    renderPage(page, index) {
        var className = this.props.current == page ? 'paginator__dir--active' : '';
        return (
            <li key={index} className={className}>
                <Link className="paginator__dir paginator__dir--page" to={this.getLink(page)}>{page}</Link>
            </li>
        );
    }

    render() {
        const {pages, current, range} = this.props;
        if ((isNaN(pages) || isNaN(current) || isNaN(range))) {
            return <ul className='paginator paginator--empty'></ul>;
        }


        let list = this.getPageRange();
        if (list.length == 1) {
            return <ul className='paginator paginator--empty'></ul>;
        }

        return (
            <div className="paginator__wrap">
                <ul className='paginator'>
                    <li className={current === 1 ? 'disabled' : ''}>
                        {this.getPrevLink(current, pages)}
                    </li>
                    {list.map(this.renderPage, this)}
                    <li className={current >= pages ? 'disabled' : ''}>
                        {this.getNextLink(current, pages)}
                    </li>
                </ul>
            </div>
        );
    }

    getPrevLink(current, pages) {
        const svgIcon = `icon-arrow-left`,
            useSvgTag = `<use xlink:href="#icon-arrow-left" />`;
        if (current > 1) {
            return <Link className="paginator__dir paginator__dir--left" to={this.getLink(+current - 1)}>
                <svg className={'paginator__icon ' + svgIcon} dangerouslySetInnerHTML={{__html: useSvgTag }} />
            </Link>
        } else {
            return <span className="paginator__dir paginator__dir--left paginator__dir--disable">
                <svg className={'paginator__icon paginator__icon--disable ' + svgIcon} dangerouslySetInnerHTML={{__html: useSvgTag }} />
            </span>
        }
    }

    getNextLink(current, pages) {
        const svgIcon = `icon-arrow-right`,
            useSvgTag = `<use xlink:href="#icon-arrow-right" />`;
        if (current < pages) {
            return <Link className="paginator__dir paginator__dir--right" to={this.getLink(+current + 1)}>
                <svg className={'paginator__icon ' + svgIcon} dangerouslySetInnerHTML={{__html: useSvgTag }} />
            </Link>
        } else {
            return <span className="paginator__dir paginator__dir--right paginator__dir--disable">
                <svg className={'paginator__icon paginator__icon--disable ' + svgIcon} dangerouslySetInnerHTML={{__html: useSvgTag }} />
            </span>
        }
    }

    getLink(page) {
        let params = {pathname: this.props.pathname};
        page ? params.query = {page: page} : false;
        return params;
    }
}

export default Paginator;
