import db from '../index';
let mongoose = require('mongoose');
import ErrorNotFound from 'components/Error/ErrorNotFound';

/**
 * Model data for sections.
 */
class SectionModel {
    /**
     * @constructor
     */
    constructor() {
        console.log('____SectionModel::constructor_____');
        /**
         * @type {{id: String, title: String}}
         */
        const schema = {
            id: String,
            title: String
        };

        this.model = mongoose.model('Section', new mongoose.Schema(schema));
    }

    /**
     * Get full list of sections.
     * @resolve {array} list
     * @rejects {object} {status: 404}
     * @return {Promise}
     */
    getList() {
        return new Promise((resolve, reject) => {
            this.model.find().exec((err, list) => {
                if (list && list.length) {
                    resolve(list);
                } else {
                    reject(new ErrorNotFound({status: 404, place: 'SectionModel.getList:37'}));
                }
            });
        });
    }

    /**
     * Get title of sections.
     * @param {number} id
     * @resolve {string} title
     * @rejects {object} {status: 404}
     * @return {Promise}
     */
    getTitle(id) {
        return new Promise((resolve, reject) => {
            try {
                this.model.findOne({id: id}).exec((err, section) => {
                    if (section && section.title) {
                        resolve(section.title);
                    } else {
                        reject(new ErrorNotFound({status: 404, place: 'SectionModel.getTitle:59'}));
                    }
                });
            } catch(e) {
                console.log(e);
            }
        });
    }
}
let SectionModelInstance = new SectionModel();
export default SectionModelInstance;
