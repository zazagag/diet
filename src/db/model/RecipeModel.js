import db from '../index';
let mongoose = require('mongoose');
let random = require('mongoose-simple-random');
import SectionsModel from 'db/model/SectionsModel';

let schema = {
    id: String,
    sectionId: String,
    title: String,
    description: String,
    images: {
        type: Array,
        default: []
    },
    original: String
};

module.exports = function() {
    let model = mongoose.model('Recipe', new mongoose.Schema(schema).plugin(random)),
        recipesPerPage = 20,

        /**
         * Normalize page number from query (start from zero).
         * @param page
         * @return {number}
         */
        normalizePage = function(page) {
            return page <= 1 ? 0 : (page - 1);
        };

    return {
        getIndexList: () => {
            return new Promise((resolve, reject) => {
                SectionsModel.getList().then((sectionList) => {
                    let sectionsIdList = [];
                    sectionList.forEach((section) => {
                        sectionsIdList.push(section.id);
                    });

                    let filter = {
                            sectionId: {
                                $in: sectionsIdList
                            }
                        },
                        fields = {},
                        options = {
                            skip: 100,
                            limit: recipesPerPage
                        };

                    model.findRandom(filter, fields, options, (err, list) => {
                        if (!err) {
                            resolve({
                                list: list,
                                meta: {
                                    title: 'Кулинария - просто!',
                                    description: 'Кулинария - просто!',
                                    keywords: 'Кулинария - просто!'
                                }
                            });
                        }
                    });
                });
            });
        },

        getSectionList: (options) => {
            const {sectionId, query: {page: page}} = options;
            return new Promise((resolve, reject) => {
                if (page && isNaN(page)) {
                    reject();
                }

                /**
                 * Number of items will skip.
                 * @type {number}
                 */
                const skip = normalizePage(page) * recipesPerPage;
                var promises = [
                    new Promise((resl, rejc) => {
                        model.find({sectionId: sectionId}).skip(skip).limit(recipesPerPage).exec((err, list) => {
                            console.log('RecipeModel.getSectionList: ' + list.length);
                            if (!list || !list.length) {
                                rejc();
                            } else {
                                resl(list);
                            }
                        });
                    }),

                    new Promise((resl, rejc) => {
                        model.find({sectionId: sectionId}).count().exec((err, count) => {
                            resl(Math.ceil(count/recipesPerPage));
                        });
                    }),

                    new Promise((resl, rejc) => {
                        SectionsModel.getTitle(sectionId).then((title) => {
                            if (title) {
                                resl(title);
                            } else {
                                rejc();
                            }
                        });
                    })
                ];

                Promise.all(promises).then((res) => {
                    const [list, pages, sectionTitle] = res;
                    resolve({
                        list: list,
                        sectionId: sectionId,
                        sectionTitle: sectionTitle,
                        meta: {
                            title: sectionTitle,
                            description: sectionTitle,
                            keywords: sectionTitle
                        },
                        pages: pages,
                        page: page || 1
                    });
                }).catch((e) => {
                    console.log('RecipeModel.js:107 catch in getSectionList');
                    console.log(e);
                    reject({status: 404});
                });
            })
        },

        getRecipeById: (options) => {
            let {sectionId, recipeId} = options;
            return new Promise((resolve, reject) => {
                var promises = [
                    new Promise((res, rej) => {
                        model.findOne({id: recipeId}).exec((err, item) => {
                            if (!item) {
                                rej();
                            } else {
                                delete item.original;
                                res(item);
                            }
                        });
                    }),

                    new Promise((res, rej) => {
                        SectionsModel.getTitle(sectionId).then((title) => {
                            if (!title) {
                                rej();
                            } else {
                                res(title);
                            }
                        });
                    })
                ];

                Promise.all(promises).then((res) => {
                    let [item, sectionTitle] = res;
                    const title = sectionTitle + '. ' + item.title;
                    resolve({
                        list: [item],
                        sectionId: sectionId,
                        sectionTitle: sectionTitle,
                        meta: {
                            title: item.title,
                            description: title,
                            keywords: title
                        }
                    });
                }).catch((e) => {
                    console.log('RecipeModel.js:109 catch in getRecipeById');
                    console.log(e);
                    reject({status: 404});
                });
            });
        }
    }
}();
