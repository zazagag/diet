let mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/test');
let db = mongoose.connection;
db.on('error', (err) => {
    console.log('db err');
    console.log(err);
}).on('disconnect', (err) => {
    console.log('db disconnect');
    console.log(err);
}).once('open', () => {
    console.log('db open');
});

export default db;
