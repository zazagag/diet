var mongoose = require('mongoose');
var translit = require('iso_9/translit');

mongoose.connect('mongodb://localhost/test');
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
    console.log('we\'re connected!');
    //load();
});

var schemaRecipe = new mongoose.Schema({
    id: String,
    sectionId: String,
    title: String,
    description: String,
    images: {
        type: Array,
        default: []
    },
    original: String
});
var RecipeModel = mongoose.model('Recipe', schemaRecipe),
    Recipe;
//RecipeModel.find().exec((err, list) => {
//    var images = [];
//    for (var recipe in list) {
//        images.push(...list[recipe].images);
//    }
//
//    fs.writeFile('./images/images.json', JSON.stringify(images), {flag: 'w'}, function(err){
//        if (err) throw err;
//        console.log('File saved.')
//    });
//});


var http = require('http'),
    fs = require('fs');

var errImages = [];

function loadAndSave(index) {
    return new Promise(function(resolve, reject) {
        var src = images[index];
        var _link = /\w+\.\w+(\/.+)$/.exec(src);
debugger;//[xxx]
        if (!_link || !_link.length) {
            fs.writeFileSync('./images/unparsed.log', src +', \r\n', {flag: 'a+'}, function(err){})
            //debugger;//[xxx]
            resolve(++index < images.length ? index : 'done');
        }

        var link = _link[1];

        var path = link.split('/');
        var dir = path[1];
        var file = path[2];

        var options = {
            host: 'img.recepti.kz',
            port: 80,
            path: link
        };

        console.log('Try load image by link: ' + link);
        http.get(options, function(res){
            var imagedata = '';
            res.setEncoding('binary');

            res.on('data', function(chunk){
                imagedata += chunk
            });

            res.on('end', function(){
                dir = './images/' + dir;
                if (!fs.existsSync(dir)){
                    fs.mkdirSync(dir);
                    console.log('Create dir: ' + dir);
                }
                var fileName = dir + '/' + file;
                console.log('Try save image by path: ' + fileName);
                fs.writeFile(fileName, imagedata, 'binary', function(err){
                    if (err)  {
                        errImages.push(images[index]);
//                        throw err;
                    } else {
                        console.log('File [' + fileName + '] saved.');
                        console.log('Image num [' + index + '] saved.');
                    }
                    fs.writeFile('./images/save.log', JSON.stringify(index), {flag: 'w'}, function(err){})
                    //debugger;//[xxx]
                    resolve(++index < images.length ? index : 'done');
                })
            })
        }).on('error', (e) => {
            console.log(`Got error: ${e.message}`);
            errImages.push(images[index]);
            //debugger;//[xxx]
            resolve(++index < images.length ? index : 'done');
        });
    })
}

function getDelay() {
    var min = 100;
    var max = 200;
    var delay = (Math.floor(Math.random() * (max - min + 1)) + min);
    console.log('delay:' + delay);
    return delay;
}

var images = require('./images/images.json');
console.log('Total images: ' + images.length);
function load(index) {
    //debugger;//[xxx]
    if (index === 'done') {
        console.log(index);
        fs.writeFile('./images/errImages.json', JSON.stringify(images), {flag: 'w'}, function(err){
            if (err) throw err;
            console.log('File saved.')
        });
        return false;
    }
    //debugger;//[xxx]
    setTimeout(function() {
        //debugger;//[xxx]
        loadAndSave(index).then(function(next) {
            //debugger;//[xxx]
            load(next)
        });
    }, getDelay());
}

//load(0);
load(3515);

/*
 error load
/4791/4791489.JPG
*/
