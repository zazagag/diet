"use strict";

var mongoose = require('mongoose');
var translit = require('iso_9/translit');

mongoose.connect('mongodb://localhost/test');
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
    console.log('we\'re connected!');
    init();
});


var init = function() {
    initSection();
    initRecipes();
};

var initSection = function() {
    var data = JSON.parse(require('./data.json'));
    var schemaSection = new mongoose.Schema({
        id: String,
        title: String
    });
    var SectionModel = mongoose.model('Section', schemaSection),
        Section;
    data.forEach(function(sect, index) {
        var link = sect.link.replace('/', '');
        Section = new SectionModel({
            id: link,
            title: sect.title
        });
        Section.save(function(err, item) {
            if (err) {
                console.error(err);
            }
        });
    });
};

var initRecipes = function() {
    var data = JSON.parse(require('./data.json')),
        listIds = [];

    var schemaRecipe = new mongoose.Schema({
        id: String,
        sectionId: String,
        title: String,
        description: String,
        images: {
            type: Array,
            default: []
        },
        original: String
    });
    var RecipeModel = mongoose.model('Recipe', schemaRecipe),
        Recipe;
    data.forEach(function(sect, index) {
        var sectionId = sect.link.replace('/', '');

        sect.recipes.forEach(function(recipeItem, index) {
            var recipeData = recipeItem.data;
            if (recipeData) {
                var title = recipeData.title.trim() || (console.log(recipeItem)) && '';

                if (!title) {
                    return false;
                }

                var transliteId = translit(
                    title
                        .replace(/[^a-zA-Zа-яА-Я\s]/g, '')
                        .replace(/[\s]/g, '-')
                        .toLowerCase(),
                    1).replace(/[^a-zA-Zа-яА-Я\-]/g, '');

                var count = 0;
                listIds.forEach(function(savedId)  {
                    if (transliteId == savedId.replace(/\-[\d]+$/, '')) {
                        count++;
                    }
                });

                if (count) {
                    transliteId += `-${count}`;
                    console.log(transliteId);
                }

                listIds.push(transliteId);
                Recipe = new RecipeModel({
                    id: transliteId,
                    sectionId: sectionId,
                    title: title,
                    description: recipeData.text,
                    images: getImages(recipeData.images),
                    original: recipeItem.link
                });
                Recipe.save(function(err, item) {
                    if (err) {
                        console.error(err);
                    }
                });
            } else {
                console.log(recipeItem);
            }
        });
    });

    RecipeModel.find()
};

function getImages(images) {
    if (!images) {
        return images;
    }
    if (!images.map) {
        return images;
    }
    return images.map((src) => {
        let match = /([\w\d_-]*)\.?[^\\\/]*$/.exec(src);
        return match[0].replace('.JPG', '');
    })
}
