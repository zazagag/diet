export const SET_IS_FROM_SERVER = 'SET_IS_FROM_SERVER';
export const setIsFromServer = (isServer) => {
    return {
        type: SET_IS_FROM_SERVER,
        isServer
    }
};

export const GET_MENU_LIST = 'GET_MENU_LIST';
export const getMenuList = (list) => {
    return {
        type: GET_MENU_LIST,
        list
    }
};

export const REQUEST_INDEX_RECIPE_LIST = 'REQUEST_INDEX_RECIPE_LIST';
export const requestIndexRecipeList = () => {
    return {
        type: REQUEST_INDEX_RECIPE_LIST
    }
};

export const RECEIVE_INDEX_RECIPE_LIST = 'RECEIVE_INDEX_RECIPE_LIST';
export const receiveIndexRecipeList = (data) => {
    return {
        type: RECEIVE_INDEX_RECIPE_LIST,
        list: data.list,
        meta: data.meta
    }
};

export const REQUEST_SECTION_RECIPE_LIST = 'REQUEST_SECTION_RECIPE_LIST';
export const requestSectionRecipeList = () => {
    return {
        type: REQUEST_SECTION_RECIPE_LIST
    }
};

export const RECEIVE_SECTION_RECIPE_LIST = 'RECEIVE_SECTION_RECIPE_LIST';


/**
 * Receive section items.
 * @param {Object} data
 * @return {{type: string, list: *, sectionId: *, sectionTitle: *, meta: (*|meta|{title, description, keywords}), pages: *, page: (*|page)}}
 */
export const receiveSectionRecipeList = (data) => {
    return {
        type: RECEIVE_SECTION_RECIPE_LIST,
        list: data.list,
        sectionId: data.sectionId,
        sectionTitle: data.sectionTitle,
        meta: data.meta,
        pages: data.pages,
        page: data.page
    }
};

export const REQUEST_RECIPE_ITEM = 'REQUEST_RECIPE_ITEM';
/**
 * Request recipe item.
 * @param {String} recipeId
 * @return {{type: string, ajaxDone: boolean}}
 */
export const requestRecipeItem = (recipeId) => {
    return {
        type: REQUEST_RECIPE_ITEM,
        ajaxDone: false
    }
};

export const RECEIVE_RECIPE_ITEM = 'RECEIVE_RECIPE_ITEM';
/**
 * Receive recipe item.
 * @param data
 * @return {{type: string, item: *, meta: (*|meta|{title, description, keywords}), sectionId: *, sectionTitle: *, ajaxDone: boolean}}
 */
export const receiveRecipeItem = (data) => {
    return {
        type: RECEIVE_RECIPE_ITEM,
        list: data.list,
        meta: data.meta,
        sectionId: data.sectionId,
        sectionTitle: data.sectionTitle,
        ajaxDone: true
    }
};

export const SET_ERROR = 'SET_ERROR';
/**
 * Set error.
 * @param error
 * @return {{type: string, status: number}}
 */
export const setError = (error) => {
    return {
        type: SET_ERROR,
        error: error
    }
};
