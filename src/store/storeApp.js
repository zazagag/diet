import reducerMenu from 'store/reducers/reducerMenu';
import reducerPage from 'store/reducers/reducerPage';
import reducerError from 'store/reducers/reducerError';
import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunkMiddleware from 'redux-thunk';

/**
 * Redux store object.
 * @class StoreApp
 */
class StoreApp {
    /**
     * @param {object} state
     * @constructor
     */
    constructor(state = {}) {
        this.store = createStore(
            combineReducers({
                reducerMenu,
                reducerPage,
                reducerError
            }),
            ...state,
            applyMiddleware(
                thunkMiddleware
            )
        );
    }

    /**
     * Return Redux store.
     * @return {object}
     */
    getStore() {
        return this.store;
    }
}

export default StoreApp;
