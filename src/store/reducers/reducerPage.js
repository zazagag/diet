import * as Actions from 'store/actions';

const reducerPage = (state = [], action) => {
    switch (action.type) {
        case Actions.REQUEST_INDEX_RECIPE_LIST:
            console.log('____Actions.REQUEST_INDEX_RECIPE_LIST');
            return Object.assign({}, state, {
                ajaxDone: false
            });
        case Actions.RECEIVE_INDEX_RECIPE_LIST:
            console.log('____Actions.RECEIVE_INDEX_RECIPE_LIST');
            return Object.assign({}, state, {
                content: {
                    list: action.list,
                    meta: action.meta
                },
                ajaxDone: true
            });
        case Actions.REQUEST_SECTION_RECIPE_LIST:
            console.log('____Actions.REQUEST_SECTION_RECIPE_LIST');
            return Object.assign({}, state, {
                ajaxDone: false
            });
        case Actions.RECEIVE_SECTION_RECIPE_LIST:
            console.log('____Actions.RECEIVE_SECTION_RECIPE_LIST');
            return Object.assign({}, state, {
                content: {
                    list: action.list,
                    sectionId: action.sectionId,
                    sectionTitle: action.sectionTitle,
                    meta: action.meta,
                    pages: action.pages,
                    page: action.page
                },
                ajaxDone: true
            });
        case Actions.REQUEST_RECIPE_ITEM:
            console.log('____Actions.REQUEST_RECIPE_ITEM');
            return Object.assign({}, state, {
                ajaxDone: false
            });
        case Actions.RECEIVE_RECIPE_ITEM:
            console.log('____Actions.RECEIVE_RECIPE_ITEM');
            return Object.assign({}, state, {
                content: {
                    list: action.list,
                    sectionId: action.sectionId,
                    sectionTitle: action.sectionTitle,
                    meta: action.meta
                },
                ajaxDone: true
            });
        default:
            return state;
    }
};

export default reducerPage;
