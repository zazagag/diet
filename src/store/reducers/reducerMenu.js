import * as Actions from 'store/actions';

const reducerMenu = (state = [], action) => {
    switch (action.type) {
        case Actions.GET_MENU_LIST:
            return Object.assign({}, state, {
                list: action.list
            });
        default:
            return state;
    }
};

export default reducerMenu;
