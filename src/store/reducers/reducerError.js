import * as Actions from 'store/actions';

const reducerError = (state = [], action) => {
    switch (action.type) {
        case Actions.SET_ERROR:
            return Object.assign({}, state, {
                error: action.error
            });
        default:
            return state;
    }
};

export default reducerError;
