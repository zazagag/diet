"use strict";

var express = require('express'),
    port = 3000,
    page = require('./compiled/server.js').page,
    api = require('./compiled/server.js').api,
    app = express(),
    path = require('path');

// process.on('uncaughtException', function (error) {
//     console.log(error.stack);
// });

app.use('/api', api);
app.use(function(req, res, next) {
    page(req, res, next).then(function(options) {
       res.status(options.status).end(options.html);
    });
});

var server = app.listen(3000, function() {
    console.log('Listening on port %d', server.address().port);
});
