import React from 'react';
import { Router, Route, Link, browserHistory, match, RouterContext } from 'react-router';
import { renderToString } from 'react-dom/server';
import { Provider } from 'react-redux';
import * as Actions from 'store/actions';
import SectionsModel from 'db/model/SectionsModel';
import RecipeModel from 'db/model/RecipeModel';
import getRoutes from 'server/routes';
import StoreApp from 'store/storeApp';
import Meta from 'components/Meta/Meta';
import NotFoundPage from 'components/Page/NotFound/NotFoundPage.jsx';

let Store = new StoreApp(),
    routes = getRoutes(),

    /**
     * Load content data depends of params and query.
     * @param {Promise.resolve} resolve
     * @param {Promise.reject} reject
     * @param {object} params
     * @param {object} query
     */
    loadContent = function(resolve, reject, params, query) {
        const {sectionId, recipeId} = params;
        // loadIndexPageRecipeList
        if (!sectionId && !recipeId) {
            console.log('server.loadContent::RecipeModel.getIndexList');
            RecipeModel.getIndexList().then((data) => {
                Store.getStore().dispatch(Actions.receiveIndexRecipeList(data));
                resolve(data);
            }).catch((e) => {
                Store.getStore().dispatch(Actions.receiveIndexRecipeList([]));
                reject(e);
            });
        }

        // loadSectionPageRecipeList
        if (sectionId && !recipeId) {
            console.log('server.loadContent::RecipeModel.getSectionList');
            RecipeModel.getSectionList({sectionId: sectionId, query: query}).then((res) => {
                Store.getStore().dispatch(Actions.receiveSectionRecipeList(res));
                resolve(res);
            }).catch((e) => {
                Store.getStore().dispatch(Actions.receiveSectionRecipeList([]));
                reject(e);
            });
        }

        // loadRecipe
        if (sectionId && recipeId) {
            console.log('server.loadContent::RecipeModel.getRecipeById');
            RecipeModel.getRecipeById({sectionId: sectionId, recipeId: recipeId, query: query}).then((res) => {
                Store.getStore().dispatch(Actions.receiveRecipeItem(res));
                resolve(res);
            }).catch((e) => {
                Store.getStore().dispatch(Actions.receiveRecipeItem([]));
                reject(e);
            });
        }
    };

/**
 * Enter point of application.
 * @param {object} req
 * @param {object} res
 * @param {function} next
 * @return {Promise}
 */
const page = (req, res, next) => {
    return new Promise((resolve, reject) => {
        /**
         * Matches a set of routes to a location, without rendering.
         * @see https://github.com/reactjs/react-router/blob/master/docs/API.md#match-routes-location-history-options--cb
         */
        match({routes, location: req.url}, (err, redirectLocation, renderProps) => {
            console.log('---------------- START PAGE ----------------');

            /**
             * Load page data for server rendering.
             */
            Promise.all([
                new Promise((reslv) => {
                    /**
                     * Load menu data.
                     */
                    SectionsModel.getList().then((list) => {
                        Store.getStore().dispatch(Actions.getMenuList(list.map((item) => {
                            return {
                                url: `/${item.id}/`,
                                name: item.title,
                                sectionId: item.id
                            }
                        })));
                        reslv();
                    });
                }),
                new Promise((reslv, rejct) => {
                    loadContent(reslv, rejct, renderProps.params, req.query);
                })]
            ).then(() => {
                console.log('server/server.js __then__');
                const initialState = Store.getStore().getState();
                const componentHTML = renderToString(
                    <Provider store={Store.getStore()}>
                        <RouterContext {...renderProps} />
                    </Provider>
                );

                let MetaPresenter = new Meta(initialState.reducerPage.content.meta),
                    html = `<!DOCTYPE html>
                            <html>
                                <head>
                                    <meta charset="utf-8"/>
                                    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
                                    ${MetaPresenter.getHTML()}
                                    <link rel="stylesheet" href="/static/styles.css"/>
                                    <script>
                                        window.__INITIAL_STATE__ = ${JSON.stringify(initialState)};
                                        window.initialUrl = '${req.url}';
                                    </script>
                                </head>
                                <body>
                                    ${require('../../public/svg/icon-sprite.svg')}
                                    <div id="react__node" class="react__node" style="padding-left: calc(100vw - 100%);">${componentHTML}</div>
                                    <!--<div class="overlay overlay__preload"><img src="/images/teapot.png"/></div>-->
                                    <!--[if lte IE 8]>
                                        <div class="overlay overlay__old-browser"><h2>Ваш браузер безнадежно устарел</h2><img src="/images/teapot.png"/></div>
                                    <![endif]-->
                                    <script type="application/javascript" src="/static/app.js"></script>
                                </body>
                            </html>`;
                let {status} = renderProps.routes[renderProps.routes.length - 1];
                resolve({html: html, status: status});
            }).catch((e) => {
                console.log('server/server.js __catch__');
                console.log(e);

                const initialState = Store.getStore().getState();
                let html = `<!DOCTYPE html>
                            <html>
                                <head>
                                    <meta charset="utf-8"/>
                                    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
                                    <link rel="stylesheet" href="/static/styles.css"/>
                                    <title>404</title>
                                    <meta name="keywords" content="404">
                                    <meta name="description" content="404">
                                    <script>
                                        window.__INITIAL_STATE__ = ${JSON.stringify(initialState)};
                                        window.initialUrl = '${req.url}';
                                    </script>
                                </head>
                                <body>
                                    <div id="react__node" class="react__node" style="padding-left: calc(100vw - 100%);">${renderToString(<NotFoundPage />)}</div>                                    
                                    <script type="application/javascript" src="/static/app.js"></script>
                                </body>
                            </html>`;

                resolve({html: html, status: 404});
            })
        });
    });
};

import api from './api/api';
module.exports = {
    api: api,
    page: page
};
