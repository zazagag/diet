var express = require('express');
var router = express.Router();
import RecipeModel from 'db/model/RecipeModel';

router.get('/recipes/', function(req, res) {
    console.log(`Api.js /recipes/`);
    RecipeModel.getIndexList().then((list) => {
        res.json(list);
    });
});

router.get('/recipes/:sectionId', function(req, res) {
    console.log(`Api.js /recipes/${req.params.sectionId}`);
    RecipeModel.getSectionList({sectionId: req.params.sectionId, query: req.query}).then((list) => {
        res.json(list);
    }).catch((e) => {
        res.json(e);
    });
});

router.get('/recipes/:sectionId/:recipeId', function(req, res) {
    const {sectionId, recipeId} = req.params;
    console.log(`Api.js /recipes/${sectionId}/${recipeId}`);
    RecipeModel.getRecipeById({sectionId: sectionId, recipeId: recipeId}).then((item) => {
        res.json(item);
    }).catch((e) => {
        res.json(e);
    });
});

router.get('*', function(req, res) {
    res.json(null);
});

export default router;
