import React from 'react';
import {Router, Route, IndexRoute} from 'react-router';
import AppPage from 'components/Page/App/AppPage.jsx';
import IndexPage from 'components/Page/Index/IndexPage.jsx';
import ItemPage from 'components/Page/Item/ItemPage.jsx';
import SectionPage from 'components/Page/Section/SectionPage.jsx';
import NotFoundPage from 'components/Page/NotFound/NotFoundPage.jsx';

/**
 * @param store
 * @return {XML[]}
 */
let getRoutes = (store) => {
    /**
     * Is run on client init stage(check is window.initialUrl same as window location have).
     * @param nextState
     * @return {boolean}
     */
    let isNeedClientRender = function (nextState) {
        const initialUrl = window && window.initialUrl;
        const location = nextState.location;
        const isEquals = !!(initialUrl && initialUrl !== (location.pathname + location.search));
        window.initialUrl = '_';
        return isEquals;
    };

    /**
     * Callback on pathname change.
     * @param nextState
     * @return {boolean}
     */
    let onEnterCb = (nextState) => {
        console.log('____ onEnterCb ____');

        /**
         * If render on server go away(already have data).
         */
        if (JSON.parse(server)) {
            return false;
        }

        /**
         * Check is client init(already have and render on server).
         */
        if (!isNeedClientRender(nextState)) {
            return;
        }

        const matches = nextState.routes;
        let component = matches[matches.length -1].component;
        component.fetchData({store: store, params: nextState.params, page: nextState.location.query.page});
    };

    /**
     * Callback on query change.
     * @param prevState
     * @param nextState
     * @return {boolean}
     */
    let onChangeCb = (prevState, nextState) => {
        console.log('____ onChangeCb ____');

        /**
         * If render on server go away(already have data).
         */
        if (JSON.parse(server)) {
            return false;
        }

        /**
         * Check is client init(already have and render on server).
         */
        if (!isNeedClientRender(nextState)) {
            return;
        }

        const matches = nextState.routes;
        let component = matches[matches.length -1].component;
        component.fetchData({store: store, params: nextState.params, page: nextState.location.query.page});
    };

    return [
        <Router key={'rRouter'}>
            <Route path="/" component={AppPage} key={'rIterator'}>
                <IndexRoute status={200} component={IndexPage} onChange={onChangeCb}  onEnter={onEnterCb} key={'rIndexPage'} />
                <Route status={200} path="/:sectionId/:recipeId" component={ItemPage} onChange={onChangeCb} onEnter={onEnterCb} key={'rItemPage'} />
                <Route status={200} path="/:sectionId" component={SectionPage} onChange={onChangeCb} onEnter={onEnterCb} key={'rSectionPage'} />
            </Route>
            <Route status={404} path="*" component={NotFoundPage} key={'rNotFoundPage'} />
        </Router>
    ];
};

export default getRoutes;
